﻿using Microsoft.AspNetCore.Mvc;

using RMHedon.datamodels;
using RMHedon.viewmodels;

namespace RMHedon.api.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class apiCategoryController : ControllerBase
    {
        private readonly DB_RMHedonContext db;
        private VMResponse respon = new VMResponse();
        private int IdUser = 1;

        public apiCategoryController(DB_RMHedonContext _db)
        {
            db = _db;
        }

        [HttpGet("GetAllData")]
        public List<MMenuCategory> GetAllData()
        {
            List<MMenuCategory> data = db.MMenuCategories.Where(a => a.IsDelete == false).ToList();
            return data;
        }

        [HttpGet("CheckCategoryByName/{namecategory}/{id}")]
        public bool CheckCategoryByName(string namecategory, long id)
        {
            MMenuCategory data = new MMenuCategory();
            if (id == 0)
            {
                data = db.MMenuCategories.Where(a => a.Namecategory == namecategory && a.IsDelete == false).FirstOrDefault();
            }
            else
            {
                data = db.MMenuCategories.Where(a => a.Namecategory == namecategory && a.IsDelete == false && a.Id != id).FirstOrDefault();
            }


            if (data != null) // jika ada
            {
                return true;
            }

            return false;
        }
        [HttpPost("SaveCategory")]
        public VMResponse Save(MMenuCategory dataParam)
        {
            MMenuCategory data = new MMenuCategory();
            data.Namecategory = dataParam.Namecategory;
            data.Description = dataParam.Description;
            data.CreatedBy = dataParam.CreatedBy;
            data.CreatedOn = DateTime.Now;
            data.IsDelete = false;

            try
            {
                db.Add(data);
                db.SaveChanges();

                respon.Message = "Kategori berhasil ditambahkan";
            }
            catch (Exception ex)
            {
                respon.Success = false;
                respon.Message = "Penambahan gagal : " + ex.Message;
            }

            return respon;
        }

        [HttpGet("GetDataById/{id}")]
        public MMenuCategory GetDataById(long id)
        {
            MMenuCategory data = db.MMenuCategories.Where(a => a.Id == id).FirstOrDefault();

            return data;
        }

        [HttpPut("Edit")]// tidak pake parameter {}, pake request body karna melempar class
        public VMResponse Edit(MMenuCategory dataParam)
        {
            MMenuCategory dt = db.MMenuCategories.Where(a => a.Id == dataParam.Id).FirstOrDefault()!;

            if (dt != null)
            {
                dt.Namecategory = dataParam.Namecategory;
                dt.Description = dataParam.Description;
                dt.ModifiedBy = dataParam.ModifiedBy;
                dt.ModifiedOn = DateTime.Now;
                dt.IsDelete = false;

                try
                {
                    db.Update(dt);
                    db.SaveChanges();

                    respon.Message = "Data Save Success";
                }
                catch (Exception ex)
                {
                    respon.Success = false;
                    respon.Message = "Data Save Failed" + ex.Message;
                }
            }
            else
            {
                respon.Success = false;
                respon.Message = "Data not Found";
            }

            return respon;
        }

        [HttpDelete("Delete/{Id}/{DeletedBy}")]
        public VMResponse Delete(int Id, int DeletedBy)
        {
            MMenuCategory dt = db.MMenuCategories.Where(a => a.Id == Id).FirstOrDefault()!;

            if (dt != null)
            {
                dt.DeletedBy = DeletedBy;
                dt.DeletedOn = DateTime.Now;
                dt.IsDelete = true;

                try
                {
                    db.Update(dt);
                    db.SaveChanges();

                    respon.Message = "Data Delete Success";
                }
                catch (Exception ex)
                {
                    respon.Success = false;
                    respon.Message = "Data Delete Failed" + ex.Message;
                }
            }
            else
            {
                respon.Success = false;
                respon.Message = "Data not Found";
            }

            return respon;
        }

    }
}
