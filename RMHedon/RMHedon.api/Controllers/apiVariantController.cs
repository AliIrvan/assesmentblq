﻿using Microsoft.AspNetCore.Mvc;

using RMHedon.datamodels;
using RMHedon.viewmodels;

namespace RMHedon.api.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class apiVariantController : ControllerBase
    {
        private readonly DB_RMHedonContext db;
        private VMResponse respon = new VMResponse();
        private int IdUser = 1;

        public apiVariantController(DB_RMHedonContext _db)
        {
            db = _db;
        }

        [HttpGet("GetAllData")]
        public List<VMMMenuVariant> GetAllData()
        {
            List<VMMMenuVariant> data = (from v in db.MMenuVariants
                                         join c in db.MMenuCategories on v.IdCategory equals c.Id
                                         where v.IsDelete == false
                                         select new VMMMenuVariant
                                         {
                                             Id = v.Id,
                                             Namevariant = v.Namevariant,
                                             Namecategory = c.Namecategory,

                                             IsDelete = v.IsDelete,
                                             CreatedBy = v.CreatedBy,
                                             CreatedOn = v.CreatedOn,
                                         }).ToList();
            return data;
        }

        [HttpGet("CheckNameIsExist/{namevariant}/{id}/{idCategory}")]
        public bool CheckNameIsExist(string namevariant, long id, long idCategory)
        {
            MMenuVariant data = new MMenuVariant();
            if (id == 0)
            {
                data = db.MMenuVariants.Where(a => a.Namevariant == namevariant && a.IsDelete == false &&
                                            a.IdCategory == idCategory).FirstOrDefault()!;
            }
            else
            {
                data = db.MMenuVariants.Where(a => a.Namevariant == namevariant && a.IsDelete == false &&
                                            a.IdCategory == idCategory && a.Id != id).FirstOrDefault()!;
            }

            if (data != null)
            {
                return true;
            }

            return false;
        }

        [HttpPost("Save")]
        public VMResponse Save(MMenuVariant data)
        {
            data.CreatedBy = IdUser;
            data.CreatedOn = DateTime.Now;
            data.IsDelete = false;

            try
            {
                db.Add(data);
                db.SaveChanges();

                respon.Message = "Data save Success";
            }
            catch (Exception ex)
            {
                respon.Success = false;
                respon.Message = "Save Error " + ex.Message;
            }

            return respon;
        }

        [HttpGet("GetDataById/{id}")]
        public VMMMenuVariant GetDataById(int id)
        {
            VMMMenuVariant data = (from v in db.MMenuVariants
                                   join c in db.MMenuCategories on v.IdCategory equals c.Id
                                   where v.IsDelete == false && v.Id == id
                                   select new VMMMenuVariant
                                   {
                                       Id = v.Id,
                                       Namevariant = v.Namevariant,
                                       IdCategory = v.IdCategory,
                                       Namecategory = c.Namecategory,
                                       IsDelete = v.IsDelete,
                                       CreatedOn = v.CreatedOn,
                                       CreatedBy = v.CreatedBy

                                   }).FirstOrDefault()!;

            return data;
        }

        [HttpPut("Edit")]
        public VMResponse Edit(MMenuVariant data)
        {
            MMenuVariant dt = db.MMenuVariants.Where(a => a.Id == data.Id).FirstOrDefault()!;

            if (dt != null)
            {
                dt.Namevariant = data.Namevariant;
                dt.IdCategory = data.IdCategory;
                dt.IdCategory = data.IdCategory;
                dt.ModifiedBy = IdUser;
                dt.ModifiedOn = DateTime.Now;

                try
                {
                    db.Update(dt);
                    db.SaveChanges();

                    respon.Message = "Data Save Success";
                }
                catch (Exception ex)
                {
                    respon.Success = false;
                    respon.Message = "Data Save Failed" + ex.Message;
                }
            }
            else
            {
                respon.Success = false;
                respon.Message = "Data not Found";
            }

            return respon;
        }
        [HttpDelete("Delete/{Id}/{DeletedBy}")]
        public VMResponse Delete(long Id, long DeletedBy)
        {
            MMenuVariant dt = db.MMenuVariants.Where(a => a.Id == Id).FirstOrDefault()!;

            if (dt != null)
            {
                dt.DeletedBy = DeletedBy;
                dt.DeletedOn = DateTime.Now;
                dt.IsDelete = true;

                try
                {
                    db.Update(dt);
                    db.SaveChanges();

                    respon.Message = "Data Delete Success";
                }
                catch (Exception ex)
                {
                    respon.Success = false;
                    respon.Message = "Data Delete Failed" + ex.Message;
                }
            }
            else
            {
                respon.Success = false;
                respon.Message = "Data not Found";
            }

            return respon;
        }


    }
}
