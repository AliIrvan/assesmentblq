﻿using Microsoft.AspNetCore.Mvc;

using RMHedon.datamodels;
using RMHedon.Services;
using RMHedon.viewmodels;

namespace RMHedon.Controllers
{
    public class VariantController : Controller
    {
        private VariantService variantService;
        private CategoryService categoryService;

        private int IdUser = 1;

        public VariantController(VariantService _variantService, CategoryService _categoryService)
        {
            variantService = _variantService;
            categoryService = _categoryService;

        }
        public async Task<IActionResult> Index(string sortOrder, string searchString, string currentFilter, int? pageNumber, int? pageSize)
        {
            ViewBag.CurrentSort = sortOrder;
            ViewBag.CurrentPageSize = pageSize;
            ViewBag.NameSort = string.IsNullOrEmpty(sortOrder) ? "name_desc" : "";

            if (searchString != null)
            {
                pageNumber = 1;
            }
            else
            {
                searchString = currentFilter;
            }
            ViewBag.CurrentFilter = searchString;

            List<VMMMenuVariant> data = await variantService.GetAllData();

            if (!string.IsNullOrEmpty(searchString))
            {
                data = data.Where(a => a.Namevariant.ToLower().Contains(searchString.ToLower())).ToList();
            }

            switch (sortOrder)
            {
                case "name_desc":
                    data = data.OrderByDescending(a => a.Namevariant).ToList();
                    break;
                default:
                    data = data.OrderBy(a => a.Namevariant).ToList();
                    break;
            }
            return View(PaginatedList<VMMMenuVariant>.CreateAsync(data, pageNumber ?? 1, pageSize ?? 5));
        }

        public async Task<IActionResult> Create()
        {
            VMMMenuVariant data = new VMMMenuVariant();

            // get all data master kategori disini...
            List<MMenuCategory> listCategories = await categoryService.GetAllData();
            ViewBag.ListCategory = listCategories;

            return PartialView(data);
        }

        public async Task<JsonResult> CheckNameIsExist(string namevariant, long id, long idCategory)
        {
            bool isExist = await variantService.CheckNameIsExist(namevariant, id, idCategory);
            return Json(isExist);
        }

        [HttpPost]
        public async Task<IActionResult> Create(VMMMenuVariant dataParam)
        {

            VMResponse respon = await variantService.Create(dataParam);

            if (respon.Success)
            {
                return Json(new { dataRespon = respon });
            }
            return View(dataParam);
        }

        public async Task<IActionResult> Edit(long id)
        {
            VMMMenuVariant data = await variantService.GetDataById(id);

            List<MMenuCategory> listCategories = await categoryService.GetAllData();
            ViewBag.ListCategory = listCategories;

            return PartialView(data);
        }

        [HttpPost]
        public async Task<IActionResult> Edit(VMMMenuVariant dataParam)
        {
            VMResponse respon = await variantService.Edit(dataParam);

            if (respon.Success)
            {
                return Json(new { dataRespon = respon });
            }
            return View(dataParam);
        }
        public async Task<IActionResult> Delete(long id)
        {
            VMMMenuVariant data = await variantService.GetDataById(id);

            List<MMenuCategory> listCategories = await categoryService.GetAllData();
            ViewBag.ListCategory = listCategories;


            return PartialView(data);
        }

        [HttpPost]
        public async Task<IActionResult> SureDelete(VMMMenuVariant dataParam)
        {
            dataParam.DeletedBy = IdUser;

            VMResponse respon = await variantService.Delete(dataParam);
            if (respon.Success)
            {
                return Json(new { dataRespon = respon });
            }
            else
            {
                return RedirectToAction("Index");
            }
        }



    }
}
