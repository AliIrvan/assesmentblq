﻿using Microsoft.AspNetCore.Mvc;

using RMHedon.datamodels;
using RMHedon.Services;
using RMHedon.viewmodels;

namespace RMHedon.Controllers
{
    public class CategoryController : Controller
    {
        private CategoryService categoryService;
        private int IdUser = 1;

        public CategoryController(CategoryService _categoryService)
        {
            categoryService = _categoryService;

        }
        public async Task<IActionResult> Index(string sortOrder, string searchString, string currentFilter, int? pageNumber, int? pageSize)
        {
            ViewBag.CurrentSort = sortOrder;
            ViewBag.CurrentPageSize = pageSize;
            ViewBag.NameSort = string.IsNullOrEmpty(sortOrder) ? "name_desc" : "";

            if (searchString != null)
            {
                pageNumber = 1;
            }
            else
            {
                searchString = currentFilter;
            }
            ViewBag.CurrentFilter = searchString;

            List<MMenuCategory> data = await categoryService.GetAllData();

            if (!string.IsNullOrEmpty(searchString))
            {
                data = data.Where(a => a.Namecategory.ToLower().Contains(searchString.ToLower())).ToList();
            }

            switch (sortOrder)
            {
                case "name_desc":
                    data = data.OrderByDescending(a => a.Namecategory).ToList();
                    break;
                default:
                    data = data.OrderBy(a => a.Namecategory).ToList();
                    break;
            }
            return View(PaginatedList<MMenuCategory>.CreateAsync(data, pageNumber ?? 1, pageSize ?? 5));
        }

        public async Task<IActionResult> Create()
        {
            return PartialView();
        }
        public async Task<JsonResult> CheckNameIsExist(string namecategory, long id)
        {
            bool isExist = await categoryService.CheckNameIsExist(namecategory, id);
            return Json(isExist);
        }

        [HttpPost]
        public async Task<IActionResult> Create(MMenuCategory dataParam)
        {
            dataParam.CreatedBy = IdUser;
            VMResponse respon = await categoryService.Create(dataParam);

            if (respon.Success)
            {
                return Json(new { dataRespon = respon });
            }
            return View(dataParam);
        }
        public async Task<IActionResult> Edit(long id)
        {

            VMMMenuCategory data = await categoryService.GetDataById(id);
            return PartialView(data);
        }

        [HttpPost]
        public async Task<IActionResult> Edit(VMMMenuCategory dataParam)
        {
            dataParam.ModifiedBy = IdUser;

            VMResponse respon = await categoryService.Edit(dataParam);

            if (respon.Success)
            {
                return Json(new { dataRespon = respon });
            }
            return View(dataParam);
        }

        public async Task<IActionResult> Delete(int id)
        {
            VMMMenuCategory data = await categoryService.GetDataById(id);
            return PartialView(data);
        }

        [HttpPost]
        public async Task<IActionResult> SureDelete(VMMMenuCategory dataParam)
        {
            dataParam.DeletedBy = IdUser;

            VMResponse respon = await categoryService.Delete(dataParam);
            if (respon.Success)
            {
                return Json(new { dataRespon = respon });
            }
            else
            {
                return RedirectToAction("Index");
            }
        }
    }
}
