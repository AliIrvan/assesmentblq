﻿using Newtonsoft.Json;

using RMHedon.datamodels;
using RMHedon.viewmodels;

using System.Text;

namespace RMHedon.Services
{
    public class CategoryService
    {
        private static readonly HttpClient client = new HttpClient();
        private IConfiguration configuration;
        private string RouteAPI = "";
        private VMResponse respon = new VMResponse();
        public CategoryService(IConfiguration _configuration)
        {
            configuration = _configuration;
            RouteAPI = configuration["RouteAPI"];
        }

        public async Task<List<MMenuCategory>> GetAllData()
        {
            List<MMenuCategory> data = new List<MMenuCategory>();
            string apirespon = await client.GetStringAsync(RouteAPI + "apiCategory/GetAllData");
            data = JsonConvert.DeserializeObject<List<MMenuCategory>>(apirespon);

            return data;
        }
        public async Task<bool> CheckNameIsExist(string namecategory, long id)
        {
            string apirespon = await client.GetStringAsync(RouteAPI + $"apiCategory/CheckCategoryByName/{namecategory}/{id}");
            bool isExist = JsonConvert.DeserializeObject<bool>(apirespon);
            return isExist;
        }
        public async Task<VMResponse> Create(MMenuCategory dataParam)
        {
            // convert dari object ke string json
            string json = JsonConvert.SerializeObject(dataParam);

            // mengubah string json lalu dikirim sebagai requst body
            StringContent content = new StringContent(json, UnicodeEncoding.UTF8, "application/json");

            // proses memanggil API dan mengirimkan body
            var request = await client.PostAsync(RouteAPI + "apiCategory/SaveCategory", content);

            if (request.IsSuccessStatusCode)
            {
                //membca respon dari API
                var apiRespon = await request.Content.ReadAsStringAsync();

                //convert hasil respon dari API ke object
                respon = JsonConvert.DeserializeObject<VMResponse>(apiRespon);
            }
            else
            {
                respon.Success = false;
                respon.Message = $"{request.StatusCode} : {request.ReasonPhrase}";
            }
            return respon;
        }

        public async Task<VMMMenuCategory> GetDataById(long id)
        {
            VMMMenuCategory data = new VMMMenuCategory();

            string apiResponse = await client.GetStringAsync(RouteAPI + $"apiCategory/GetDataById/{id}");
            data = JsonConvert.DeserializeObject<VMMMenuCategory>(apiResponse);
            return data;
        }

        public async Task<VMResponse> Edit(VMMMenuCategory dataParam)
        {
            // convert dari object ke string json
            string json = JsonConvert.SerializeObject(dataParam);

            // mengubah string json lalu dikirim sebagai requst body
            StringContent content = new StringContent(json, UnicodeEncoding.UTF8, "application/json");

            // proses memanggil API dan mengirimkan body
            var request = await client.PutAsync(RouteAPI + "apiCategory/Edit", content);

            if (request.IsSuccessStatusCode)
            {
                //membca respon dari API
                var apiRespon = await request.Content.ReadAsStringAsync();

                //convert hasil respon dari API ke object
                respon = JsonConvert.DeserializeObject<VMResponse>(apiRespon);
            }
            else
            {
                respon.Success = false;
                respon.Message = $"{request.StatusCode} : {request.ReasonPhrase}";
            }
            return respon;
        }

        public async Task<VMResponse> Delete(VMMMenuCategory dataParam)
        {

            var request = await client.DeleteAsync(RouteAPI + $"apiCategory/Delete/{dataParam.Id}/{dataParam.DeletedBy}");
            //var request = await client.DeleteAsync(RouteAPI + $"apiCaraPembayaran/Delete/{id}");

            if (request.IsSuccessStatusCode)
            {
                // Proses membaca respon dari API
                var apiRespon = await request.Content.ReadAsStringAsync();

                // proses convert hasil respon dari API ke Object
                respon = JsonConvert.DeserializeObject<VMResponse>(apiRespon);
            }
            else
            {
                respon.Success = false;
                respon.Message = $"{request.StatusCode} : {request.ReasonPhrase}";
            }

            return respon;
        }


    }
}
