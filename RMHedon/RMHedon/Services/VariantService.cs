﻿using Newtonsoft.Json;

using RMHedon.viewmodels;

using System.Text;

namespace RMHedon.Services
{
    public class VariantService
    {
        private static readonly HttpClient client = new HttpClient();
        private IConfiguration configuration;
        private string RouteAPI = "";
        private VMResponse respon = new VMResponse();
        public VariantService(IConfiguration _configuration)
        {
            configuration = _configuration;
            RouteAPI = configuration["RouteAPI"];
        }

        public async Task<List<VMMMenuVariant>> GetAllData()
        {
            List<VMMMenuVariant> data = new List<VMMMenuVariant>();
            string apirespon = await client.GetStringAsync(RouteAPI + "apiVariant/GetAllData");
            data = JsonConvert.DeserializeObject<List<VMMMenuVariant>>(apirespon);

            return data;
        }

        public async Task<bool> CheckNameIsExist(string namevariant, long id, long idCategory)
        {
            string apirespon = await client.GetStringAsync(RouteAPI + $"apiVariant/CheckNameIsExist/{namevariant}/{id}/{idCategory}");
            bool isExist = JsonConvert.DeserializeObject<bool>(apirespon);
            return isExist;
        }
        public async Task<VMResponse> Create(VMMMenuVariant dataParam)
        {
            // convert dari object ke string json
            string json = JsonConvert.SerializeObject(dataParam);

            // mengubah string json lalu dikirim sebagai requst body
            StringContent content = new StringContent(json, UnicodeEncoding.UTF8, "application/json");

            // proses memanggil API dan mengirimkan body
            var request = await client.PostAsync(RouteAPI + "apiVariant/Save", content);

            if (request.IsSuccessStatusCode)
            {
                //membca respon dari API
                var apiRespon = await request.Content.ReadAsStringAsync();

                //convert hasil respon dari API ke object
                respon = JsonConvert.DeserializeObject<VMResponse>(apiRespon);
            }
            else
            {
                respon.Success = false;
                respon.Message = $"{request.StatusCode} : {request.ReasonPhrase}";
            }
            return respon;
        }
        public async Task<VMMMenuVariant> GetDataById(long id)
        {
            VMMMenuVariant data = new VMMMenuVariant();

            string apiResponse = await client.GetStringAsync(RouteAPI + $"apiVariant/GetDataById/{id}");
            data = JsonConvert.DeserializeObject<VMMMenuVariant>(apiResponse);
            return data;
        }
        public async Task<VMResponse> Edit(VMMMenuVariant dataParam)
        {
            // convert dari object ke string json
            string json = JsonConvert.SerializeObject(dataParam);

            // mengubah string json lalu dikirim sebagai requst body
            StringContent content = new StringContent(json, UnicodeEncoding.UTF8, "application/json");

            // proses memanggil API dan mengirimkan body
            var request = await client.PutAsync(RouteAPI + "apiVariant/Edit", content);

            if (request.IsSuccessStatusCode)
            {
                //membca respon dari API
                var apiRespon = await request.Content.ReadAsStringAsync();

                //convert hasil respon dari API ke object
                respon = JsonConvert.DeserializeObject<VMResponse>(apiRespon);
            }
            else
            {
                respon.Success = false;
                respon.Message = $"{request.StatusCode} : {request.ReasonPhrase}";
            }
            return respon;
        }
        public async Task<VMResponse> Delete(VMMMenuVariant dataParam)
        {

            var request = await client.DeleteAsync(RouteAPI + $"apiVariant/Delete/{dataParam.Id}/{dataParam.DeletedBy}");
            //var request = await client.DeleteAsync(RouteAPI + $"apiCaraPembayaran/Delete/{id}");

            if (request.IsSuccessStatusCode)
            {
                // Proses membaca respon dari API
                var apiRespon = await request.Content.ReadAsStringAsync();

                // proses convert hasil respon dari API ke Object
                respon = JsonConvert.DeserializeObject<VMResponse>(apiRespon);
            }
            else
            {
                respon.Success = false;
                respon.Message = $"{request.StatusCode} : {request.ReasonPhrase}";
            }

            return respon;
        }


    }
}
