﻿namespace RMHedon.viewmodels
{
    public class VMMMenuCategory
    {
        public long Id { get; set; }
        public string Namecategory { get; set; } = null!;
        public long CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public long? ModifiedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public long? DeletedBy { get; set; }
        public DateTime? DeletedOn { get; set; }
        public bool IsDelete { get; set; }
        public string? Description { get; set; }
    }
}
