CREATE DATABASE portalakademik

CREATE TABLE m_mahasiswa(
	id INT PRIMARY KEY IDENTITY(1,1),
	namamahasiswa VARCHAR(50) NOT NULL,
	semester INT NOT NULL,
	jeniskelamin varchar(1),  
	created_by bigint not null,
    created_on datetime not null,
    modified_by bigint null,
    modified_on datetime null,
    deleted_by bigint null,
    deleted_on datetime null,
    is_delete bit not null
)

CREATE TABLE m_dosen(
	id INT PRIMARY KEY IDENTITY(1,1),
	namadosen VARCHAR(50) NOT NULL,
	created_by bigint not null,
    created_on datetime not null,
    modified_by bigint null,
    modified_on datetime null,
    deleted_by bigint null,
    deleted_on datetime null,
    is_delete bit not null
)

CREATE TABLE m_matakuliah(
	id INT PRIMARY KEY IDENTITY(1,1),
	namamatakuliah VARCHAR(MAX) NOT NULL,
	dosen VARCHAR(100) not null,
	created_by bigint not null,
    created_on datetime not null,
    modified_by bigint null,
    modified_on datetime null,
    deleted_by bigint null,
    deleted_on datetime null,
    is_delete bit not null
)

CREATE TABLE m_kelas(
	id INT PRIMARY KEY IDENTITY(1,1),
	namakelas VARCHAR(5) not null,
	mahasiswa_id int not null,
	dosen_id int not null,
	matakuliah_id int not null,
	created_by bigint not null,
    created_on datetime not null,
    modified_by bigint null,
    modified_on datetime null,
    deleted_by bigint null,
    deleted_on datetime null,
    is_delete bit not null
)

CREATE TABLE m_nilai(
	id int PRIMARY KEY IDENTITY(1,1),
	mahasiswa_id int not null,
	matakuliah_id int not null,
	dosen_id int not null,
	nilai int not null,
	created_by bigint not null,
    created_on datetime not null,
    modified_by bigint null,
    modified_on datetime null,
    deleted_by bigint null,
    deleted_on datetime null,
    is_delete bit not null
)

INSERT INTO m_nilai(mahasiswa_id, matakuliah_id, dosen_id, nilai, created_by, created_on, is_delete)
VALUES
	(1, 1, 1, 90, 1, GETDATE(), 0),
	(2, 1, 1, 85, 1, GETDATE(), 0)


	SELECT * FROM m_nilai
