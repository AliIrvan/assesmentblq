﻿char[] trip = { 'N', 'N', 'T', 'N', 'N', 'N', 'T', 'T', 'T', 'T', 'T', 'N', 'T', 'T', 'T', 'N', 'T', 'N' };
int stat = 0;

List<int> tripHistory = new List<int>();

int gunung = 0;
int lembah = 0;

for (int i = 0; i < trip.Length; i++)
{
    if (trip[i] == 'N')
    {
        stat += 1;
        tripHistory.Add(stat);
    }
    else if (trip[i] == 'T')
    {
        stat += -1;
        tripHistory.Add(stat);
    }
    else
    {
        Console.WriteLine("Hattori nyasar");
    }

    if (stat == 0)
    {
        if (tripHistory.Max() > 0)
        {
            gunung++;
            tripHistory.Clear();
        }
        else if (tripHistory.Min() < 0)
        {
            lembah++;
            tripHistory.Clear();
        }
    }
}

Console.WriteLine($"Jumlah gunung = {gunung} ");
Console.WriteLine($"Jumlah Lembah = {lembah} ");