﻿Console.Write("Masukkan jarak awal : ");
int jarakAwal = int.Parse(Console.ReadLine());

Console.Write("Suit A 3 Round (pemisah : space) : ");
string[] suitA = Console.ReadLine().ToUpper().Split(" ");

Console.Write("Suit B  3 Round (pemisah : space) : ");
string[] suitB = Console.ReadLine().ToUpper().Split(" ");

int round = 3;
int posA = 0;
int posB = 0 + jarakAwal;
int jarak = Math.Abs(posA - posB);

string winner = "";
for (int i = 0; i < round; i++)
{
    if ((suitA[i] == "G" && suitB[i] == "K") || (suitA[i] == "K" && suitB[i] == "B") || (suitA[i] == "B" && suitB[i] == "G"))
    {
        posA += 2;
        posB += 1;
        jarak = Math.Abs(posA - posB);
        if (jarak == 0)
        {
            winner = "A";
            Console.WriteLine($"Pemenang : {winner}");
            break;
        }

    }
    else if ((suitB[i] == "G" && suitA[i] == "K") || (suitB[i] == "K" && suitA[i] == "B") || (suitB[i] == "B" && suitA[i] == "G"))
    {
        posA -= 1;
        posB -= 2;
        jarak = Math.Abs(posA - posB);
        if (jarak == 0)
        {
            winner = "B";
            Console.WriteLine($"Pemenang : {winner}");
            break;
        }

    }
    else if (suitA[i] == suitB[i])
    {
        continue;
    }

}

jarak = Math.Abs(posA - posB);
if (jarak != 0)
{
    Console.WriteLine("Hasil : draw");
}


