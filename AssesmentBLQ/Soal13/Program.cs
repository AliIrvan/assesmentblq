﻿Console.Write("Masukkan Jam (hh:mm) : ");
DateTime jam = DateTime.Parse(Console.ReadLine());

int sudut1 = (jam.Hour * 5) * 6;
int sudut2 = jam.Minute * 6;

int totalSudut = Math.Max(sudut1, sudut2) - Math.Min(sudut1, sudut2);

Console.WriteLine($"sudut yang terbentuk : {totalSudut}");