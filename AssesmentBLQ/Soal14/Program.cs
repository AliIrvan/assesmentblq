﻿Console.Write("Masukan deret (Pemisah : , ) : ");
int[] deret = Array.ConvertAll(Console.ReadLine().Split(","), int.Parse);

Console.Write("Masukkan Nilai N : ");
int n = int.Parse(Console.ReadLine());


for (int j = 1; j <= n; j++) {
    int temp = 0;
    temp = deret[0];
    for (int i = 0; i < deret.Length - 1; i++)
    {
        deret[i] = deret[i + 1];
    }
    deret[deret.Length - 1] = temp;

    Console.WriteLine($"Output {j} : {String.Join(", ", deret)}");
}
