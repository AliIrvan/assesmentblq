﻿Console.Write("Masukkan Kata : ");
string teks = Console.ReadLine().ToLower();

int jumHuruf = teks.Length;
int batasTengah = jumHuruf / 2;

string isPalindrome = "Palindrome";

for (int i = 0; i < jumHuruf; i++)
{
    if (teks[i] != teks[jumHuruf - 1 - i])
    {
        isPalindrome = "bukan Palindrome";
    }
}

Console.WriteLine($"Kata yang dimasukkan {isPalindrome}");
