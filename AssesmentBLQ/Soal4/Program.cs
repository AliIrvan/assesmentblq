﻿Console.Write("Masukkan Bilangan : ");
int bilangan = int.Parse(Console.ReadLine());

bool isPrime = false;
List<int> prime = new List<int>();

for (int i = 1; i <= bilangan; i++)
{
    if (i <= 3 || i == 5 || i == 7)
    {
        prime.Add(i);
    }
    else if (i % 2 == 0)
    {
        continue;
    }
    else if (i % 3 == 0)
    {
        continue;
    }
    else if (i % 5 == 0)
    {
        continue;
    }
    else if (i % 7 == 0)
    {
        continue;
    }
    else
    {
        prime.Add(i);
    }

}

Console.WriteLine($"Bilangan Prima : {String.Join(", ", prime)}");
Console.WriteLine($"Jumlah Bilangan Prima : {prime.Count}");