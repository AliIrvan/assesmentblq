﻿int[] deret = { 8, 7, 0, 2, 7, 1, 7, 6, 3, 0, 7, 1, 3, 4, 6, 1, 6, 4, 3 };

double mean, median, mod;

// Mean
mean = deret.Sum() / deret.Length;

// Median
Array.Sort(deret);
if (deret.Length % 2 == 0)
{
    median = (deret[deret.Length / 2] - deret[(deret.Length / 2) - 1]) / 2;
}
else
{
    median = deret[(deret.Length / 2) - 1];
}

//Modus
Dictionary<int, int> deretGroup = new Dictionary<int, int>();

foreach (int angka in deret)
{
    if (deretGroup.ContainsKey(angka))
    {
        deretGroup[angka]++;
    }
    else
    {
        deretGroup[angka] = 1;
    }
}

int hasilMode = int.MinValue;
int NilaiMod = int.MinValue;
foreach (int key in deretGroup.Keys)
{
    if (deretGroup[key] > NilaiMod)
    {
        NilaiMod = deretGroup[key];
        hasilMode = key;
    }
}


Console.WriteLine($"Mean = {mean}");
Console.WriteLine($"Median = {median}");
Console.WriteLine($"Mode = {hasilMode}");

