﻿Console.Write("Masukkan Waktu : ");
string waktu = Console.ReadLine();
DateTime waktudt = DateTime.Now;
string output = "";

if (waktu.Contains("PM"))
{
    waktu = waktu.Remove(waktu.Length - 2);
    waktudt = DateTime.Parse(waktu);
    output = waktudt.AddHours(12).ToString("HH:mm:ss");
}
else if(waktu.Contains("AM"))
{
    waktu = waktu.Remove(waktu.Length - 2);
    waktudt = DateTime.Parse(waktu);
    output = waktudt.ToString("HH:mm:ss");
}
else
{
    output = "Input Error";
}

Console.WriteLine($"Output : {output}");
