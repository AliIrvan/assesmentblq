﻿Console.Write("Masukkan jumlah deret : ");
int deretLength = int.Parse(Console.ReadLine());

Console.Write("Masukkan angka pertama : ");
int n1 = int.Parse(Console.ReadLine());

Console.Write("Masukkan angka kedua : ");
int n2 = int.Parse(Console.ReadLine());

int n3 = 0;

List<int> deretFibo = new List<int>();
deretFibo.Add(n1);
deretFibo.Add(n2);

for (int i = 2;i < deretLength; i++)
{
    n3 = n1 + n2;
    deretFibo.Add(n3);
    n1 = n2;
    n2 = n3;
}

Console.WriteLine($"\nDeret : {String.Join(", ", deretFibo)}");

Console.Write($"\nMasukkan deret lilin sebanyak {deretLength} (pemisah : , ) : ");
int[] panjangLilin = Array.ConvertAll(Console.ReadLine().Split(","), int.Parse);

Console.WriteLine($"\nDeret Lilin : {String.Join(", ", panjangLilin)}");

while (true)
{
    for (int i = 0; i < panjangLilin.Length; i++)
    {
        panjangLilin[i] = panjangLilin[i] - deretFibo[i];
        if (panjangLilin[i] <= 0)
        {
            Console.WriteLine($"\nLilin yg pertama habis = lilin ke {Array.IndexOf(panjangLilin, panjangLilin[i]) + 1}");
            break;
        }
    }
    break;
}









