﻿int[] deret = { 1, 2, 1, 3, 4, 7, 1, 1, 5, 6, 1, 8 };

for (int i = 0; i < deret.Length - 1; i++)
{
repeat:
    if (deret[i] > deret[i + 1])
    {
        int x = deret[i];
        deret[i] = deret[i + 1];
        deret[i + 1] = x;

        Console.WriteLine(String.Join(", ", deret));
        i = 0;
        goto repeat;
    }
}

Console.WriteLine(String.Join(", ", deret));