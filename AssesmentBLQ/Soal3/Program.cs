﻿Console.Write("Masukkan tanggal dan jam masuk (dd/MM/yyyy hh/mm/ss): ");
DateTime masuk = DateTime.Parse(Console.ReadLine());

Console.Write("Masukkan tanggal dan jam keluar (dd/MM/yyyy hh/mm/ss): ");
DateTime keluar = DateTime.Parse(Console.ReadLine());

TimeSpan lamaParkir = keluar - masuk;

int tarif = 0;


if (lamaParkir.TotalSeconds > (24 * 3600))
{
    tarif = 1500 + (lamaParkir.Hours - 24) * 1000;
}
else if (lamaParkir.TotalSeconds > (8 * 3600))
{
    tarif = 8000;
}
else
{
    tarif = lamaParkir.Hours * 1000;
}

Console.WriteLine($"Lama Parkir : {lamaParkir.ToString()}");
Console.WriteLine($"Tarif Parkir : {tarif.ToString("C")}");