﻿Console.Write("Masukkan Nama : ");
string[] nama = Console.ReadLine().Split(" ");

List<char> penggalanNama = new List<char>();
foreach (string kata in nama)
{
    for (int i = 0; i < kata.Length; i++)
    {
        if (i == 0 || i == kata.Length - 1)
        {
            penggalanNama.Add(kata[i]);
        }
        else
        {
            penggalanNama.Add('*');
        }
    }
    penggalanNama.Add(' ');
}

Console.WriteLine($"Output : {String.Join("", penggalanNama)}");