﻿string namaBukuA = "A";
int durasiA = 14;

string namaBukuB = "B";
int durasiB = 3;

string namaBukuC = "C";
int durasiC = 7;

string namaBukuD = "D";
int durasiD = 7;

Console.Write("Masukkan Tanggal Peminjaman (dd/MM/yyyy): ");
DateTime tglPinjam = DateTime.Parse(Console.ReadLine());

Console.Write("Masukkan Tanggal Pengembalian (dd/MM/yyyy): ");
DateTime tglKembali = DateTime.Parse(Console.ReadLine());

TimeSpan lamaPeminjaman = tglKembali - tglPinjam;

int dendaBukuA = ((lamaPeminjaman.Days - durasiA) < 0) ? 0 : (lamaPeminjaman.Days - durasiA) * 100;
int dendaBukuB = ((lamaPeminjaman.Days - durasiB) < 0) ? 0 : (lamaPeminjaman.Days - durasiB) * 100;
int dendaBukuC = ((lamaPeminjaman.Days - durasiC) < 0) ? 0 : (lamaPeminjaman.Days - durasiC) * 100;
int dendaBukuD = ((lamaPeminjaman.Days - durasiD) < 0) ? 0 : (lamaPeminjaman.Days - durasiD) * 100;

Console.WriteLine($"\nLama Peminjaman = {lamaPeminjaman.Days} hari");
Console.WriteLine("\nDenda Buku : ");
Console.WriteLine($"Denda Buku A sebesar {dendaBukuA.ToString("C")}");
Console.WriteLine($"Denda Buku B sebesar {dendaBukuB.ToString("C")}");
Console.WriteLine($"Denda Buku C sebesar {dendaBukuC.ToString("C")}");
Console.WriteLine($"Denda Buku D sebesar {dendaBukuD.ToString("C")}");
