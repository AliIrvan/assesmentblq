﻿using System.Security.Cryptography;

float[] order = {42, 50, 30, 70, 30 };
int totalCustomer = 4;
float[] allergicfood = { order[0] };
int allergic = 1;
float bill = 0.0F;

float pajak = 0.1F;
float service = 0.05F;

for (int i = 0;i < totalCustomer; i++)
{
    if (i < allergic)
    {
        foreach (float ord in order)
        {
            if (allergicfood.Contains(ord))
            {
                bill += 0;
            }
            else
            {
                bill += ord + (ord * pajak) + (ord * service);
            }
        }

        Console.WriteLine($"Total bill Customer {i + 1} (allergic) : {bill}");
        bill = 0.0F;

    }
    else
    {
        foreach (float ord in order)
        {
            bill += ord + (ord * pajak) + (ord * service);
        }

        Console.WriteLine($"Total bill Customer {i + 1} : {bill}");
        bill = 0.0F;

    }
}