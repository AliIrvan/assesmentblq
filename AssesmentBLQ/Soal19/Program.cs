﻿Console.Write("Masukkan Kalimat : ");
string teks = Console.ReadLine().ToLower();
string alphabet = "abcdefghijklmnopqrstuvwxyz";
List<char> huruf = new List<char>();
foreach (char abj in teks)
{
    if (char.IsLetter(abj))
    {
        huruf.Add(abj);
    }
}

string isPangram = "Pangram";

foreach (char x in alphabet)
{
    if (huruf.IndexOf(x) == -1)
    {
        isPangram = "Bukan Pangram";
    }
}

Console.WriteLine($"Kalimat tersebut {isPangram}");
