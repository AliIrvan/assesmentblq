CREATE DATABASE DB_RMHedon

CREATE TABLE M_MenuCategory(
	id BIGINT PRIMARY KEY IDENTITY (1,1),
	namecategory VARCHAR(50) not null,
	created_by bigint not null,
    created_on datetime not null,
    modified_by bigint null,
    modified_on datetime null,
    deleted_by bigint null,
    deleted_on datetime null,
    is_delete bit not null
)

ALTER TABLE M_MenuCategory
ADD [description] VARCHAR(100) null


INSERT INTO M_MenuCategory(namecategory, created_by, created_on, is_delete)
VALUES
	('Makanan', 1, GETDATE(), 0),
	('Minuman', 1, GETDATE(), 0)


SELECT * from M_MenuCategory